package betters.betterfonbet.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import static betters.betterfonbet.util.global_functions.GlobalUtilFunctions.waitForMachine;

@Service
@RequiredArgsConstructor
@Slf4j
public class BetAmountSetterService {
    private final WebDriverWait waitSettings;

    public void pressFastBet() {
        WebElement fastBetButton = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath("//div[@class='toggle--Vz5RH' and @data-component-part='toggle']")));
        fastBetButton.click();
        waitForMachine(1);
    }

    public void pressAndSetFastBetAmount(Long amountOfBet) {
            try {
                WebElement fastBetButton = waitSettings.until(ExpectedConditions.
                        presenceOfElementLocated(By.xpath("//div[@class='toggle--Vz5RH' and @data-component-part='toggle']")));
                fastBetButton.click();
                waitForMachine(1);



                WebElement writeAmountField = waitSettings.until(ExpectedConditions.
                        presenceOfElementLocated(By.xpath("//input[@class='coupon-settings-sum__editor--KgfuY' and @data-testid='stringEdit']")));
//              Очистить поле, выделив весь текст и удалив его
                writeAmountField.sendKeys(Keys.CONTROL + "a");
                waitForMachine(1);
                writeAmountField.sendKeys(Keys.DELETE);
                waitForMachine(2);

                // Теперь можно использовать sendKeys() для ввода нового текста
                writeAmountField.sendKeys(amountOfBet.toString());
                waitForMachine(1);

                log.info("Включили быструю ставку на сумму: {}", amountOfBet);
            } catch (Exception ex) {
                waitForMachine(4);
                log.error("Быстрая ставка не установлена: {}", ex.getMessage());
            }
    }
}
