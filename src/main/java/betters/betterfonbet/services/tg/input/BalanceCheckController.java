package betters.betterfonbet.services.tg.input;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component("/balance")
@RequiredArgsConstructor
public class BalanceCheckController implements BotController {
    private final WebDriverWait waitSettings;

    @Override
    public SendMessage handleRequest(Update update) {
        long chatId = update.getMessage().getChatId();
        WebElement balanceButtonBeforeBet =
                waitSettings.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class= 'sum--YJr_4']")));
        long accountBalance = Long.parseLong(balanceButtonBeforeBet.getText().replaceAll(" ", ""));


        return buildAnswer(chatId, accountBalance);
    }


    @SneakyThrows
    private SendMessage buildAnswer(long chatId, long accountBalance) {

        String rsMsg = """
                Баланс на аккаунте: %s рублей
                """.formatted(accountBalance);
        SendMessage answer = new SendMessage();
        answer.setChatId(chatId);
        answer.setText(rsMsg);
        return answer;
    }
}
