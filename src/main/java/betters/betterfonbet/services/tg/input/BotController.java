package betters.betterfonbet.services.tg.input;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface BotController {
    SendMessage handleRequest(Update update);
}
