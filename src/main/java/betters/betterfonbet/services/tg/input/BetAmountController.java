package betters.betterfonbet.services.tg.input;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component("/betAmount")
@Slf4j
@RequiredArgsConstructor
public class BetAmountController implements BotController {
    @Value("${bet.amount}")
    private Long betAmount;

    @Override
    public SendMessage handleRequest(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        String sb = "Сумма ставки: " + betAmount;
        sendMessage.setText(sb);
        return sendMessage;
    }
}
