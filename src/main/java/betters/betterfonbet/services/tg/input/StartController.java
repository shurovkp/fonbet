package betters.betterfonbet.services.tg.input;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component("/start")
@Slf4j
@RequiredArgsConstructor
public class StartController implements BotController {
    @Override
    public SendMessage handleRequest(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText("Привет. Пока можешь пользоваться командами /balance и /betAmount");
        return sendMessage;
    }
}
