package betters.betterfonbet.services.tg.input;


import betters.betterfonbet.services.tg.TelegramPropertyProvider;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class BotInitializer extends TelegramLongPollingBot {
    private final TelegramPropertyProvider telegramPropertyProvider;
    @Autowired
    private Map<String, BotController> botCommands;

    /**
     * Метод, обрабатывающий обращения к боту
     */
    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String msg = update.getMessage().getText();
            execute(botCommands.get(msg).handleRequest(update));
        }
    }

    /**
     * Инициализация бота
     */
    @Override
    public String getBotToken() {
        return telegramPropertyProvider.getBotToken();
    }

    @Override
    public String getBotUsername() {
        return telegramPropertyProvider.getBotName();
    }
}
