package betters.betterfonbet.services.tg;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Slf4j
@ConfigurationProperties(prefix = "telegram")
public class TelegramPropertyProvider {
    private String botName;
    private String botToken;
    private String chatId;
}
