package betters.betterfonbet.services.tg.output;

import betters.betterfonbet.services.tg.input.BotInitializer;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;

import java.io.File;
import java.util.List;

@RequiredArgsConstructor
@Component
@Slf4j
public class ActionTgNotifier {
    private final BotInitializer botInitializer;
    @Value("${telegram.chat-id}")
    private Long botsChatId;

    @SneakyThrows
    @Async
    public void notifyWithEventBlocksText( List<WebElement> allGames) {
        notifyWithMsg("-------------------------------------------------------", 395347375L);
        allGames.forEach(el ->notifyWithMsg(el.getText(), 395347375L));
        notifyWithMsg("-------------------------------------------------------", 395347375L);
    }

    @SneakyThrows
    @Async
    public void notifyWithEventBlocksText(String msg, Long chatId) {
        notifyWithMsg(msg, chatId);
        notifyWithMsg(msg, chatId);
    }

    @SneakyThrows
    public void notifyWithMsg(String notifyMsg) {
        SendMessage message = new SendMessage();
        message.setChatId(botsChatId);
        message.setText(notifyMsg);
        botInitializer.execute(message);
    }

    @SneakyThrows
    public void notifyWithMsg(String notifyMsg, Long chatId) {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(notifyMsg);
        botInitializer.execute(message);
    }

    @SneakyThrows
    public void notifyWithMsgNScreenshot(String notifyMsg, String screenshotPath) {
        // Создаем сообщение с текстом
        SendMessage message = new SendMessage();
        message.setChatId(botsChatId);
        message.setText(notifyMsg);
        botInitializer.execute(message);

        // Создаем и отправляем фото
        SendPhoto photo = new SendPhoto();
        photo.setChatId(botsChatId);
        photo.setPhoto(new InputFile(new File(screenshotPath)));
        botInitializer.execute(photo);
    }



}
