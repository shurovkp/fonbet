package betters.betterfonbet.services.scheduled;


import betters.betterfonbet.services.AuthorizationService;
import betters.betterfonbet.services.BetAmountSetterService;
import betters.betterfonbet.services.LanguageChanger;
import betters.betterfonbet.services.BetMakerService;
import betters.betterfonbet.util.aop.chrome.ChromeSync;
import betters.betterfonbet.util.global_functions.GlobalUtilFunctions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static betters.betterfonbet.util.global_functions.GlobalUtilFunctions.waitForMachine;


@Slf4j
@RequiredArgsConstructor
@Service
public class WebPageScheduling {
    private final WebDriver chromeDriver;
    private final AuthorizationService authorizationService;
    private final BetAmountSetterService betAmountSetterService;
    private final LanguageChanger languageChanger;
    private final GlobalUtilFunctions globalUtilFunctions;
    private final BetMakerService betMakerService;

    @Value("${bet.amount}")
    private Long betAmount;
    @Value("${urls.initial-url}")
    private String initialUrl;


    @Scheduled(fixedRate = 86_400_000)
    @ChromeSync
    public void AuthorizeAndSetFastBetEvery24HoursJob() {
        // chromeDriver.navigate().to("https://2ip.ru/");
        chromeDriver.navigate().to(initialUrl);
        languageChanger.changeLanguage();
        authorizationService.authorize();
        waitForMachine(2);
        betAmountSetterService.pressAndSetFastBetAmount(betAmount);
    }

//    @Scheduled(fixedDelay = 300_000, initialDelay = 300_000)
//    @ChromeSync
//    public void navigateToMatchPage() {
//        globalUtilFunctions.navigateToInitialUrlAndWait1Sec();
//        betAmountSetterService.pressFastBet();
//    }

//    @Scheduled(fixedDelay = 300_00)
//    @ChromeSync
//    public void makeABet() {
//        betMakerService.makeABet( "senior");
//    }

}

