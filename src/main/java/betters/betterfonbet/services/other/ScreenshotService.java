package betters.betterfonbet.services.other;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Service;

import java.io.File;

@Slf4j
@RequiredArgsConstructor
@Service
public class ScreenshotService {
    private final WebDriver driver;

    @SneakyThrows
    public String makeScreenshot(String action, String playerName) {
        // Путь к директории для сохранения скриншота
        String dirPath = "/tmp/fon-screens";
        double random = Math.random();

        // Путь и имя файла скриншота, используя только время для имени файла
        String filePath = String.format("%s\\%s_%f.2_%s.png", dirPath, playerName, random, action);

        // Создание скриншота
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        // Сохранение файла скриншота
        FileUtils.copyFile(screenshot, new File(filePath));

        System.out.println("Скриншот сохранен: " + filePath);
        return filePath;
    }
}
