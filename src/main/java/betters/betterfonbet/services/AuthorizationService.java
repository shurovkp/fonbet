package betters.betterfonbet.services;

import betters.betterfonbet.configs.chrome.ClientProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import static betters.betterfonbet.util.global_functions.GlobalUtilFunctions.waitForMachine;


@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorizationService {
    private final ClientProperties clientProperties;
    private final WebDriverWait waitSettings;
    private final WebDriver webDriver;

    public void authorize() {
        boolean userLoggedIn = isUserLoggedIn();

        if (userLoggedIn) {
           return;
        }
        login();
        log.info("Авторизовались успешно");
    }


    private boolean isUserLoggedIn() {
        try {
            waitSettings.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Войти']")));
            return false;
        } catch (RuntimeException ex) {
            return true;
        } finally {
            Actions action = new Actions(webDriver);
            action.sendKeys(Keys.ESCAPE).build().perform();
        }
    }

    private void login() {
        // Вызываем окно авторизации
        WebElement authorizationWindow = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath("//span[text()='Войти']")));
        authorizationWindow.click();
        waitForMachine(2);

        //Вводим креденшлс
        WebElement usernameInput = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath("//input[@name='login']")));
        usernameInput.sendKeys(clientProperties.getLogin()); //Login
        WebElement passwordInput = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath("//input[@type='password']")));
        passwordInput.sendKeys(clientProperties.getPassword()); //Password

        //Входим
        WebElement loginButton = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath
                        ("//span[contains(@class, 'button--_ckCX _primary--xaCqa _sizeL--cOYoD _hasText--a86Tm _interactive--hyuU0') and text()='Войти']")));
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", loginButton);
       // loginButton.click();
        waitForMachine(2);
    }
}
