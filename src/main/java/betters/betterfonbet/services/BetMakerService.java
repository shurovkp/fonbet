package betters.betterfonbet.services;

import betters.betterfonbet.services.tg.output.ActionTgNotifier;
import betters.betterfonbet.util.aop.chrome.ChromeSync;
import betters.betterfonbet.util.aop.screenshot.Screenshot;
import betters.betterfonbet.util.exceptions.BetNotMadeException;
import betters.betterfonbet.util.exceptions.PlayerNotFoundException;
import betters.betterfonbet.util.global_functions.GlobalUtilFunctions;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import java.util.List;

import static betters.betterfonbet.util.global_functions.GlobalUtilFunctions.waitForMachine;

@Service
@RequiredArgsConstructor
@Slf4j
public class BetMakerService {
    private final WebDriverWait waitSettings;
    private final GlobalUtilFunctions globalUtilFunctions;
    private final BetAmountSetterService betAmountSetterService;
    private final ActionTgNotifier tgNotifier;


    @SneakyThrows
    @Screenshot
    @ChromeSync
    public void makeABet(String playerName1) {
        globalUtilFunctions.navigateToInitialUrlAndWait2Sec();
        betAmountSetterService.pressFastBet();
        waitForMachine(1);
        WebElement ourMatchLink = findOurMatch(playerName1);
        WebElement totalLessButton = findTotalLessButton(ourMatchLink);
        totalLessButton.click();

        tgNotifier.notifyWithEventBlocksText(findAllGames());
        checkIfBetWasMade();
    }

    private WebElement findOurMatch(String playerName1) {
        List<WebElement> allGames = findAllGames();
        return allGames.stream()
                .filter(el -> el.getText().toLowerCase().contains(playerName1))
                .findFirst()
                .orElseThrow(() -> new PlayerNotFoundException("Не нашли пару c игроком: %s ".formatted(playerName1)));
    }

    private List<WebElement> findAllGames() {
        List<WebElement> allGames =
                waitSettings.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                        By.xpath("//div[contains(@class, 'sport-base-event--W4qkO _compact--eaFtY')]")));

        log.info("ПОЛУЧАЕМ СПИСОК ВСЕХ ИГР allGames:");
        allGames.forEach(el ->log.info(el.getText()));

        if (allGames.isEmpty()) {
            throw new PlayerNotFoundException("На странице нет наших матчей");
        }
        return allGames;
    }

    private WebElement findTotalLessButton(WebElement ourMatchLink) {
        return ourMatchLink.findElement(By.xpath(".//div[@data-testid='factorValue.931']"));
    }

    private void checkIfBetWasMade() {
        String balanceXpath = "//div[@class='fader--ddlZ7 _relative--TTwjI']";

        WebElement balanceB4BetBtn = waitSettings.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(balanceXpath)));
        String balanceB4Bet = balanceB4BetBtn.getText();

        waitForMachine(20);

        WebElement balanceAfterBetBtn = waitSettings.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(balanceXpath)));
        String balanceAfterBet = balanceAfterBetBtn.getText();

        if (balanceAfterBet.equals(balanceB4Bet))
            throw new BetNotMadeException("не выполнилась ставка, баланс не изменился)");
    }
}


