package betters.betterfonbet.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class LanguageChanger {
    private final WebDriverWait waitSettings;
    private final WebDriver chromeDriver;

    public void changeLanguage() {
        WebElement windowButton = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath("//span[@data-testid='btn.languages']")));
        ((JavascriptExecutor) chromeDriver).executeScript("arguments[0].click();", windowButton);

        WebElement russianButton = waitSettings.until(ExpectedConditions.
                presenceOfElementLocated(By.xpath("//span[text()='Русский']")));
        ((JavascriptExecutor) chromeDriver).executeScript("arguments[0].click();", russianButton);

        log.info("Изменили язык на русский");
    }
}