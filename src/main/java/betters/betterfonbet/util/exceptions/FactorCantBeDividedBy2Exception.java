package betters.betterfonbet.util.exceptions;

public class FactorCantBeDividedBy2Exception extends RuntimeException{
    public FactorCantBeDividedBy2Exception(String message) {
        super(message);
    }

}
