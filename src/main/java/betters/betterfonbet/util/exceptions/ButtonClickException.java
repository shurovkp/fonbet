package betters.betterfonbet.util.exceptions;

public class ButtonClickException extends RuntimeException {
    public ButtonClickException(String message) {
        super(message);
    }
}
