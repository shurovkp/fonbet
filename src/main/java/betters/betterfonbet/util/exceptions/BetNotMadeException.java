package betters.betterfonbet.util.exceptions;

public class BetNotMadeException extends RuntimeException {
    public BetNotMadeException(String message) {
        super(message);
    }

}
