package betters.betterfonbet.util.exceptions;

public class NoAvailableTotalLessButtonsException extends RuntimeException {
    public NoAvailableTotalLessButtonsException(String message) {
        super(message);
    }

}

