package betters.betterfonbet.util.global_functions;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GlobalUtilFunctions {
    private final WebDriverWait waitSettings;
    private final WebDriver chromeDriver;

    @Value("${urls.initial-url}")
    private String initialUrl;


    @SneakyThrows //Ждем через поток, хз почему, но через waitSettings не всегда срабатывает
    public static void waitForMachine(Integer amountOfSeconds) {
        Thread.sleep(amountOfSeconds * 1000);
    }

    public void navigateToInitialUrlAndWait2Sec() {
        boolean pageIsEmpty;

        do {
            chromeDriver.navigate().to(initialUrl);
            pageIsEmpty = checkIfPageIsEmpty();

        } while (pageIsEmpty);

        waitForMachine(2);
    }

    private boolean checkIfPageIsEmpty() {
        try {
            waitSettings.until(ExpectedConditions.
                    presenceOfElementLocated(By.xpath("//a[@data-testid='event']")));
            return false;
        } catch (Exception ex) {
            return true;
        }
    }
}
