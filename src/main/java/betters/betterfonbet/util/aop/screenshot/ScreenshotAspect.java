package betters.betterfonbet.util.aop.screenshot;

import betters.betterfonbet.services.other.ScreenshotService;
import betters.betterfonbet.services.tg.output.ActionTgNotifier;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class ScreenshotAspect {
    private final ScreenshotService screenshotService;
    private final ActionTgNotifier tgNotifier;
    @Pointcut("@annotation(betters.betterfonbet.util.aop.screenshot.Screenshot)")
    public void notifyPointcut() {
    }

    @SneakyThrows
    @Around("notifyPointcut()")
    public void aroundAdvice(ProceedingJoinPoint joinPoint) {
        String nickname = (String) joinPoint.getArgs()[0];

        try {
            String screenshotPath = screenshotService.makeScreenshot("b4GetMatches",nickname);
            tgNotifier.notifyWithMsgNScreenshot("Before bet", screenshotPath);

            joinPoint.proceed();
        } finally {
            String screenshotPath = screenshotService.makeScreenshot("AfterClick", nickname);
            tgNotifier.notifyWithMsgNScreenshot("After bet", screenshotPath);

        }
    }
}
