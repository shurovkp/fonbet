package betters.betterfonbet.util.aop.chrome;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;


@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class ChromeSyncAspect {
    private final WebDriver chromeDriver;
    @Pointcut("@annotation(betters.betterfonbet.util.aop.chrome.ChromeSync)")
    public void notifyPointcut() {
    }

    @SneakyThrows
    @Around("notifyPointcut()")
    public void aroundAdvice(ProceedingJoinPoint joinPoint){
        synchronized (chromeDriver) {
                joinPoint.proceed();
        }
    }
}