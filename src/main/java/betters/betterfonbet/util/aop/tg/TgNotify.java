package betters.betterfonbet.util.aop.tg;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TgNotify {
    boolean onFail() default true;
    boolean onSuccess() default false;
}
