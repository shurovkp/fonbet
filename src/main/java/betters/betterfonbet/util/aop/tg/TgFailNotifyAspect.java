package betters.betterfonbet.util.aop.tg;

import betters.betterfonbet.services.BetAmountSetterService;
import betters.betterfonbet.services.tg.output.ActionTgNotifier;
import betters.betterfonbet.util.global_functions.GlobalUtilFunctions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class TgFailNotifyAspect {
    private final ActionTgNotifier tgNotifier;
    private final WebDriverWait waitSettings;
    private final WebDriver webDriver;
    private final BetAmountSetterService betAmountSetterService;
    private final GlobalUtilFunctions globalUtilFunctions;


    @Pointcut("@annotation(betters.betterfonbet.util.aop.tg.TgNotify)")
    public void notifyPointcut() {
    }

    @Around("notifyPointcut()")
    public void aroundAdvice(ProceedingJoinPoint joinPoint){
        synchronized (webDriver) {
            globalUtilFunctions.navigateToInitialUrlAndWait2Sec();
            betAmountSetterService.pressFastBet();
        }

        try {
            String balanceBeforeBet = getBalance();
            tgNotifier.notifyWithMsg("Будет выполнена ставка, баланс перед ставкой: %s".formatted(balanceBeforeBet));

            joinPoint.proceed();

            String balanceAfterBet = getBalance();
            tgNotifier.notifyWithMsg("Успех, баланс после ставки: %s".formatted(balanceAfterBet));
        } catch (Throwable ex) {
            log.info("\nОшибка : {},", ex.getMessage(), ex.getCause());
            tgNotifier.notifyWithMsg("Ошибка: %s".formatted(ex.getMessage()));
        }
    }

    private String getBalance() {
            WebElement balanceWebElement = waitSettings
                    .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class= 'sum--YJr_4']")));
            return balanceWebElement.getText();
    }

}