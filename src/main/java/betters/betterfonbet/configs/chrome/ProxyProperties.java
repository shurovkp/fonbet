package betters.betterfonbet.configs.chrome;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "proxy")
@Component
@Getter
@Setter
public class ProxyProperties {
    private String host;
    private int port;
    private String login;
    private String password;
}
