package betters.betterfonbet.configs.chrome;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "client-info")
@Component
@Getter
@Setter
@Slf4j
public class ClientProperties {
    private String login;
    private String password;

    @PostConstruct
    public void printNameOfAccAndLogin() {
       log.info("login: {}", login);
    }
}
