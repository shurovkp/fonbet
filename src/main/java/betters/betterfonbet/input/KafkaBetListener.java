package betters.betterfonbet.input;

import betters.betterfonbet.dto.ConditionsForBet;
import betters.betterfonbet.services.BetMakerService;
import betters.betterfonbet.util.aop.tg.TgNotify;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class KafkaBetListener {
    private final ObjectMapper om;
    private final BetMakerService betMakerService;

    @SneakyThrows
    @KafkaListener(topics = "betters-topic", groupId = "#{'${spring.kafka.groupId}'}")
    @TgNotify
    void listener(String condition) {
        log.info("Полчили сообщение о необходимости ставки: {}", condition);
        ConditionsForBet conditionsForBet = om.readValue(condition, ConditionsForBet.class);
        betMakerService.makeABet(conditionsForBet.getPlayer1());
    }
}
