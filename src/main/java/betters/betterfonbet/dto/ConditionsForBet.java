package betters.betterfonbet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConditionsForBet {
    private String player1;
    private String player2;
    private String minute;
}
