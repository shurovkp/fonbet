# Этап 1: Создаем временный контейнер для сборки проекта с помощью Gradle
FROM gradle:latest AS builder

# Устанавливаем рабочую директорию внутри контейнера сборки
WORKDIR /app

# Копируем файлы с зависимостями и Gradle build.gradle и settings.gradle
COPY build.gradle settings.gradle ./

# Копируем исходный код приложения
COPY src/ src/

# Собираем проект
RUN gradle build

# Этап 2: Создаем контейнер для выполнения приложения собранного на предыдущем этапе
FROM openjdk:17

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем JAR-файл из временного контейнера сборки
COPY --from=builder /app/build/libs/better-fonbet-1.jar app

# Команда для запуска приложения при запуске контейнера
CMD ["java", "-jar", "app"]